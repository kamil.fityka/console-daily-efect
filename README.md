# Standalone console application

## Build project
1. Run `docker-compose build` to build project
2. Run `docker-compose up -d` to start dockers

## Command
1. Go into php container `docker exec -it console-project-php-1 bash`
2. Run command:
   1. For local file e.x.: 
      `php bin/console app:process-xml-file local file:///var/www/app/data/ coffee.xml` or
      `php bin/console app:process-xml-file local data/ coffee.xml`
   2. For remote file e.x.: `php bin/console app:process-xml-file remote ftp://pupDev:pupDev2018@transport.productsup.io/ coffee_feed_trimmed.xml`

## Additional info:
1. Docker ports, passwords, users are in `.env` file.
2. Docker test environment is defined in `env.test`

## Test
1. For test run command `php vendor/bin/phpunit tests`