<?php

declare(strict_types=1);

namespace App\CreateOrder\EventHandler;

use App\Application\CreateOrder\Event\CreateOrderMessage;

#[\Attribute(\Attribute::TARGET_CLASS | \Attribute::TARGET_METHOD)]
class CreateOrderMessageHandler
{
    public function __invoke(CreateOrderMessage $message)
    {
        dump($message);
    }
}