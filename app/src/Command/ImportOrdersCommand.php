<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Reader\WordpressReader;
use App\Service\Writer\Writer;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:import')]
final class ImportOrdersCommand extends Command
{
    public function __construct(
        private WordpressReader $reader,
        private Writer $writer,
        private ?string $name = null,
    ) {
        parent::__construct($this->name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /* TODO:
            1. Postawić console
            2. Podpiąć się do bazy danych
                2.1. Wybrać odpowiednie rekordy
            3. Połączyć z nową bazą danych
                3.1. Zapisać do nowej bazy danych
                3.2. Pobrać wszystkie rekordy
                3.3. Stworzyć maile
                3.4. Wepchnąć do messenger
            4. Wysyłka maili
         */

        try {
            $output->writeln('Start import process');

            $records = $this->reader->read();

            $output->writeln(sprintf('Found %d records', $records->count()));
            $output->writeln('Start creating messages');

            $this->writer->write($records, $output);

            $output->writeln('All records transferred');

        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
