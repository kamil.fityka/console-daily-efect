<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\MessageCreator\EmailCreator;
use App\Service\MessageSender\MessageSender;
use App\Service\Reader\DoctrineReader;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(name: 'app:message')]
final class CreateMessageCommand extends Command
{
    public function __construct(
        private DoctrineReader $reader,
        private EmailCreator   $emailGenerator,
        private MessageSender $sender,
        private ?string        $name = null,
    ) {
        parent::__construct($this->name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /* TODO:
                3.2. Pobrać wszystkie rekordy
                3.3. Stworzyć maile
                3.4. Wepchnąć do messenger
         */
        try {
            $output->writeln('Start create templates');

            $records = $this->reader->read(1);

            $output->writeln(sprintf('Found %d records', $records->count()));
            $output->writeln('Start creating messages');

            $emails = $this->emailGenerator->create($records, $output);

            $this->sender->send($emails);

            $output->writeln('Finished creating messages');

        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
