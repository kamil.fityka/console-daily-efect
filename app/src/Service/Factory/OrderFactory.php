<?php

declare(strict_types=1);

namespace App\Service\Factory;

use App\Entity\Console\Order;

class OrderFactory
{
    public function create(array $order): Order
    {
        return (new Order())
            ->setEmail($order['email'])
            ->setName($order['email'])
            ->setTitle($order['post_title'])
            ->setSecondName($order['email'])
            ->setLink("")
            ;
    }
}