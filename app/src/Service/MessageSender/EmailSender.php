<?php

declare(strict_types=1);

namespace App\Service\MessageSender;

use Iterator;
use Symfony\Component\Mailer\MailerInterface;

class EmailSender implements MessageSender
{
    public function __construct(
        private MailerInterface $mailer
    )
    {
    }

    public function send(Iterator $notifications): void
    {
        while ($notifications->valid()) {

            $this->mailer->send($notifications->current());

            $notifications->next();
        }
    }
}