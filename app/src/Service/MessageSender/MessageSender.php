<?php

declare(strict_types=1);

namespace App\Service\MessageSender;

use Iterator;

interface MessageSender
{
    public function send(Iterator $notifications): void;
}