<?php

declare(strict_types=1);

namespace App\Service\Writer;

use App\Service\Factory\OrderFactory;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use ArrayIterator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;

final class DoctrineWriter implements Writer
{

    public function __construct(
        private EntityManagerInterface $em,
        private OrderFactory $orderFactory
    )
    {
    }

    public function write(ArrayIterator $records, OutputInterface $output): void
    {
        $output->writeln('Start transfer');
        $this->em->getConnection()->beginTransaction();

        try {
            while ($records->valid()) {
                $order = $this->orderFactory->create($records->current());
                $this->em->persist($order);

                if (0 === ($records->key() % 2000)) {
                    $output->writeln(sprintf('Transferring %s orders', $records->key()));
                }

                $records->next();
            }


            $output->writeln('Transfer end');
            $this->em->flush();
            $this->em->getConnection()->commit();
            $output->writeln('Saving and closing connection');

        } catch (\Exception $e) {
            $output->writeln('Something went wrong, transaction rolled back');
            $this->em->getConnection()->rollBack();
            throw $e;
        }
    }
}