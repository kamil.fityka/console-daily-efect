<?php

declare(strict_types=1);

namespace App\Service\Writer;

use ArrayIterator;
use Symfony\Component\Console\Output\OutputInterface;

interface Writer
{
    public function write(ArrayIterator $records, OutputInterface $output): void;
}