<?php

declare(strict_types=1);

namespace App\Service\Reader;

use Doctrine\ORM\EntityManagerInterface;
use ArrayIterator;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Iterator;

final class WordpressReader implements Reader
{
    private ObjectManager $em;
    private LoggerInterface $logger;

    public function __construct(
        ManagerRegistry $managerRegistry,
    )
    {
        $this->em = $managerRegistry->getManager('wordpress');
        $this->logger = new Logger('console');
        $this->logger->pushHandler(new StreamHandler('var/log/console.log'));
    }

    public function read(): ArrayIterator
    {
        try {
            $connect = $this->em->getConnection();

            $sql = '
                SELECT dp.id, dp.post_title, c.email, c.first_name, c.last_name FROM wordpress.daily_posts AS dp
                INNER JOIN daily_wc_order_stats AS os ON os.order_id = dp.id
                INNER JOIN daily_wc_customer_lookup AS c ON c.customer_id = os.customer_id
                WHERE post_type = "shop_order"
            ';

            return new ArrayIterator($connect->executeQuery($sql, [])->fetchAllAssociative());

        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
            throw $e;
        }
    }
}