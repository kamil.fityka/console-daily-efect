<?php

declare(strict_types=1);

namespace App\Service\Reader;

use ArrayIterator;

interface Reader
{
    public function read(): ArrayIterator;
}