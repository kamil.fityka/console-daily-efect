<?php

declare(strict_types=1);

namespace App\Service\Reader;

use App\Entity\Console\Order;
use ArrayIterator;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;

final class DoctrineReader implements Reader
{
    private const RECORDS_NUMBER = 30000;
    private ObjectManager $em;

    public function __construct(
        ManagerRegistry $managerRegistry,
    )
    {
        $this->em = $managerRegistry->getManager();
    }

    public function read(int $iteration = 1): ArrayIterator
    {
        $offset = ($iteration * self::RECORDS_NUMBER) - self::RECORDS_NUMBER;

        return new ArrayIterator($this->em->getRepository(Order::class)->findByInterval($offset, self::RECORDS_NUMBER));
    }
}