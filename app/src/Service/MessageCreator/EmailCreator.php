<?php

declare(strict_types=1);

namespace App\Service\MessageCreator;

use App\Entity\Console\Order;
use ArrayIterator;
use Iterator;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mime\Address;
use Twig\Environment;

class EmailCreator implements MessageCreator
{
    public function __construct(
        private Environment $twig,
        private LoggerInterface $logger
    )
    {
    }

    public function create(ArrayIterator $orders, OutputInterface $output): Iterator
    {
        while ($orders->valid()) {

            $index = $orders->key();
            $order = $orders->current();
            $template = $this->twig->render(
                name: 'email.html.twig',
                context: [
                    'order' => $order
                ]
            );

            try {
                $form = new Address('kamil.fityka@gmail.com');
                $to = new Address($order['email'], sprintf('%s %s', $order['name'], $order['secondName']));

                yield (new TemplatedEmail())
                    ->from($form)
                    ->to($to)
                    ->subject('Zmiana adresu z którego można pobrać zakupione ebooki.')
                    ->html($template)
                ;

                if (0 === ($index % 1000)) {
                    $output->writeln(sprintf('%s', $index));
                }
            } catch (\Exception $exception) {
                $output->writeln(sprintf('Exception occur. More information in log. %s', $exception->getMessage()));
                $this->logger->error(sprintf('Exception: %s', $exception->getMessage()));
                $this->logger->error(sprintf('Problem with sending email to: %s. Data: %s', $order['email'], json_encode($order)));
            }

            $orders->next();
        }
    }
}