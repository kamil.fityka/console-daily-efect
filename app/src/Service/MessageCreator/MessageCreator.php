<?php

declare(strict_types=1);

namespace App\Service\MessageCreator;

use ArrayIterator;
use Iterator;
use Symfony\Component\Console\Output\OutputInterface;

interface MessageCreator
{
    public function create(ArrayIterator $orders, OutputInterface $output): Iterator;
}